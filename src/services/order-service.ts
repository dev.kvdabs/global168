import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "./auth-service";

@Injectable()
export class OrderService {
    constructor(private db: AngularFirestore, private authService: AuthService) {}

    getOrders() {
        return this.db.collection('orders', ref => ref.orderBy('delivery_date', 'desc'));
    }

    async addOrder(order: any) {
        const id = this.db.createId();
        order['id'] = id;
        order['created_at'] = new Date().getTime();

        order.deliverer_id = this.authService.uid();
        order.deliverer_name = (await this.authService.profile()).name;

        await this.db.collection('orders').doc(id).set(order);

        return id;
    }

    async updateOrder(order: any) {
        return this.db.collection('orders').doc(order.id).update(order);        
    }
}