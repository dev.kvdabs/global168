import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "./auth-service";

@Injectable()
export class CollectionService {
    source = 'collections';

    constructor(private db: AngularFirestore, private authService: AuthService) {}

    async add(collection: any) {
        const id = this.db.createId();
        collection['id'] = id;
        collection['created_at'] = new Date().getTime();

        collection.collector_id = this.authService.uid();
        collection.collector_name = (await this.authService.profile()).name;

        await this.db.collection(this.source).doc(id).set(collection);

        return id;
    }

    async update(order: any) {
        return this.db.collection(this.source).doc(order.id).update(order);        
    }
}