import { Injectable } from "@angular/core";
import { ToastController, LoadingController } from "ionic-angular";

@Injectable() 
export class AlertService {
    constructor(private toastCtrl: ToastController, private loadingCtrl: LoadingController) {}

    toast(message: string) {
        this.toastCtrl.create({
            message: message,
            duration: 5000,
            showCloseButton: true            
        }).present();
    }

    loading(content: string) {
        return this.loadingCtrl.create({
            content: content,
            showBackdrop: true
        })
    }
}