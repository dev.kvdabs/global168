import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { Storage } from "@ionic/storage";
import { IProfile } from "../app/app.models";
import { NavController, App } from "ionic-angular";
import { LoginPage } from "../pages/login/login";

@Injectable()
export class AuthService {
    constructor(private afAuth: AngularFireAuth, private db: AngularFirestore, private storage: Storage, private app: App) {
        
    }

    currentUser() {
        return this.afAuth.auth.currentUser;
    }

    uid(): string {
        return this.afAuth.auth.currentUser.uid;
    }

    authenticate(email: string, password: string) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    getProfile() {
        return this.db.collection('profiles').doc(this.currentUser().uid).get().toPromise();
    }

    async profile(): Promise<IProfile> {      
        const json_user = await this.storage.get('current_firebase_user');
        
        if (json_user) {
            return (JSON.parse(json_user).profile as IProfile);
        }

        return {
            id: '',
            username: '',
            password: '',
            name: '',
            user_type: '',
            active: false
        }
    }
    
    updateProfile(data: any) {
        return this.db.collection('profiles').doc(this.uid()).update(data);
    }

    logout() {
        this.afAuth.auth.signOut();        
    }

    async isAdmin() {
        const user = await this.profile();
        return (user.user_type == "admin");
    }

    async isAdminStaff() {
        const user = await this.profile();
        return (user.user_type == "admin_staff");
    }

    async isAgent() {
        const user = await this.profile();
        return user.user_type == "sales_agent";
    }

    async isDeliverer() {
        const user = await this.profile();
        return user.user_type == "deliverer";
    }

    async isCollector() {
        const user = await this.profile();
        return user.user_type == "collector";
    }

    async changePassword(password: string) {
        const user = this.currentUser();
        return user.updatePassword(password);
    }
}