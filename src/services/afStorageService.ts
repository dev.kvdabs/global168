import { Injectable } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";

import { AlertService } from "./alert-service";

@Injectable()
export class AFStorageService {
    constructor(private afStorage: AngularFireStorage, private alertService: AlertService) {

    }

    download(url: string, filename: string): Promise<Blob> {
        const $this = this;
        try {
            //const ref = this.afStorage.storage.refFromURL(url);
            //return ref.getDownloadURL().then(url => {
                 return new Promise( (resolve, reject) => {
                    try {
                        var xhr = new XMLHttpRequest();
                        xhr.responseType = 'blob';
                        
                        xhr.onload = function() {
                            var blob = xhr.response;
                            resolve(blob)
                        };

                        xhr.open('GET', url);
                        xhr.send();
                    } catch (error) {
                        reject(error)
                    }
                 })
            //});
        } catch (error) {
            alert(JSON.stringify(error))
        }
    }
}