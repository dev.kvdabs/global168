import { Injectable } from "@angular/core";
import { Camera, CameraOptions } from "@ionic-native/camera";

@Injectable()
export class CameraService {
    options: CameraOptions = {
        quality: 80,
        targetHeight: 800,
        targetWidth: 800,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE        
    };

    constructor(private camera: Camera) {

    }

    async takePicture(useCamera: boolean) {
        this.options.sourceType = useCamera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY;

        const imageData = await this.camera.getPicture(this.options);      
        return 'data:image/jpeg;base64,' + imageData;  
    }
}