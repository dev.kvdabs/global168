import { Component } from "@angular/core";
import { AuthService } from "../../../services/auth-service";
import { AlertService } from "../../../services/alert-service";
import { NavController } from "ionic-angular";

@Component({
    selector: 'change-password-page',
    templateUrl: 'change-password.html'
})

export class ChangePasswordPage {
    oldPassword = '';
    newPassword = ''
    constructor(private authService: AuthService, private alertService: AlertService, private nav: NavController) {}

    async save() {        
        const loading = this.alertService.loading('Processing...');

        try {
            if (this.oldPassword.trim().length < 6) return this.alertService.toast('Current password must be atleast 6 characters');
            if (this.newPassword.trim().length < 6) return this.alertService.toast('New password must be atleast 6 characters');

            loading.present();
            const profile = await this.authService.profile();

            const email = profile.username + '@global168.com';
            const user = await this.authService.authenticate(email, this.oldPassword);
            if (user) {
                await this.authService.changePassword(this.newPassword);
                await this.authService.updateProfile({
                    password: this.newPassword
                });

                this.alertService.toast('Password updated successfully');

                this.nav.pop();
            }
            
        } catch (error) {
            if (error.code == 'auth/wrong-password') return this.alertService.toast('Invalid current password');
            this.alertService.toast('An error occured while updating password. Please try again')
        } finally {
            if (loading) loading.dismiss();
        }
    }

}