import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth-service';
import { ChangePasswordPage } from './change-password/change-password';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  name = '';

  constructor(private nav: NavController, private app: App, private authService: AuthService) {
  }

  async ionViewDidLoad() {
    this.name = (await this.authService.profile()).name;
  }

  changePassword() {
    this.nav.push(ChangePasswordPage, {}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  logout() {
    this.authService.logout();
  }
}