import { Component, ViewChild, ElementRef } from "@angular/core";
import { ILocation } from "../../../app/app.models";
import { NavParams, Platform } from "ionic-angular";
import { PhotoViewer } from "@ionic-native/photo-viewer";

@Component({
    selector: 'photo-page',
    templateUrl: 'photo.html'
})

export class PhotoPage {
    photo: string;
    deg = 360;
    rotation = "rotate(360deg)";

    constructor(private params: NavParams, private platform: Platform, private photoViewer: PhotoViewer) {
        this.photo = this.params.get('photo');        
    }

    ionViewDidLoad() {

    }

    rotate() {
        // if (this.platform.is('cordova')) {
        //     if (this.deg == 360) this.deg = 90;
        //     else this.deg += 90;

        //     this.rotate = `rotate(${this.deg}deg)`;
        // }
        // else window.open(photo, '_blank');        

        if (this.deg == 360) this.deg = 90;
        else this.deg += 90;

        this.rotation = `rotate(${this.deg}deg)`;
    }    
}