import { Component, ViewChild, ElementRef } from "@angular/core";
import { ILocation } from "../../app/app.models";
import { NavParams, Platform, NavController } from "ionic-angular";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { PhotoPage } from "./photo/photo";

@Component({
    selector: 'photos-page',
    templateUrl: 'photos.html'
})

export class PhotosPage {
    title: string;
    photos = [];
    isWeb: boolean;

    constructor(private params: NavParams, private nav: NavController, private platform: Platform, private photoViewer: PhotoViewer) {
        this.photos = this.params.get('photos') || [];
        this.title = this.params.get('title');

        this.isWeb = !this.platform.is('cordova');
    }

    ionViewDidLoad() {

    }

    viewPhoto(photo) {
        if (this.platform.is('cordova')) this.photoViewer.show(photo, '');
        else {
            this.nav.push(PhotoPage, { photo })
        }        
    }

    
}