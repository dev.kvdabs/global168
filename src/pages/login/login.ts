import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { AuthService } from '../../services/auth-service';
import { AlertService } from '../../services/alert-service';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  email: string = '';
  password: string = '';
  showLogin: boolean;

  constructor(public navCtrl: NavController, 
              private authService: AuthService,                             
              private params: NavParams,              
              private alertService: AlertService) {

    this.showLogin = this.params.get('showForm');    
  }

  login() {        
    if (this.email.trim() && this.password.trim()) this.authenticate();
  }

  async authenticate() {    
    const loading = this.alertService.loading('Logging in...');
    loading.present();

    try {
      this.authService.logout(); // logout existing user to trigger auth state

      const email = this.email + '@global168.com';
      await this.authService.authenticate(email, this.password);

    } catch (error) {
      let msg = 'Invalid username and password OR please check internet connection';
      if (error.code == 'auth/network-request-failed') {
        msg = 'Please check internet connection.'
      } else if (error.code == 'auth/user-not-found' || error.code == 'auth/wrong-password') {
        msg = 'Invalid username and password. Please try again.'
      }

      this.alertService.toast(msg);
      console.error(error);
    } finally {
      loading.dismiss();
    }
  }
}
