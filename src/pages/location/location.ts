import { Component, ViewChild, ElementRef } from "@angular/core";
import { ILocation } from "../../app/app.models";
import { NavParams } from "ionic-angular";

declare var google;

@Component({
    selector: 'location-page',
    templateUrl: 'location.html'
})

export class LocationPage {
    title: string;
    location: ILocation;

    @ViewChild('map') mapElement: ElementRef;
    map: any;

    constructor(private params: NavParams) {
        this.location = this.params.get('location');
        this.title = this.params.get('title');
    }

    ionViewDidLoad() {       

        let latLng = new google.maps.LatLng(this.location.latitude, this.location.longitude);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            disableDefaultUI: true,
            fullScreenControl: false
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter()
        });

        //this.getAddress(latLng);
    }

    getAddress(latLng) {
        var geocoder = new google.maps.Geocoder;

        geocoder.geocode({'location': latLng}, function(results, status) {
            if (status === 'OK') {
              if (results[0]) {
                // map.setZoom(11);
                // var marker = new google.maps.Marker({
                //   position: latlng,
                //   map: map
                // });
                // infowindow.setContent(results[0].formatted_address);
                // infowindow.open(map, marker);
                console.log(results[0].formatted_address)
              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Geocoder failed due to: ' + status);
            }
          });
    }
}