import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { OrderDetailPage } from './order-detail/order-detail';
import { AngularFirestore } from '@angular/fire/firestore';
import { OrderFilterPage } from './order-filter/order-filter';
import { Storage } from '@ionic/storage';
import { firestore } from 'firebase';
import { shareReplay, first } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../services/auth-service';
import { LocationPage } from '../location/location';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  ordersSubscription: Subscription;
  orders: any[];
  isAdmin: boolean;
  hasLoaded: boolean;
  _applyFilter: boolean;

  constructor(public navCtrl: NavController, private db: AngularFirestore, private storage: Storage, private events: Events, private authService: AuthService) {
    this.events.subscribe('order_filter_changed', () => {
      this._applyFilter = true;
      this.getOrders();
    });    
  }

  async ionViewDidLoad() {
    this.isAdmin = await this.authService.isAdmin() || await this.authService.isAdminStaff();

    this.getOrders();
  }

  applyFilter() {
    this.getOrders();
  }

  async getOrders() {
    
    try {    
      this.hasLoaded = false;

      const _filters = await this.storage.get('order_filters');
      const filters = _filters ? JSON.parse(_filters) : null;

      const ordersRef = this.db.collection('orders', ref => {
        let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        
        if (this._applyFilter && filters) {
          query = query.orderBy('delivered_date', filters.newest_to_oldest ? 'desc' : 'asc');
          
          if (!filters.show_all_dates) {
            const start = new Date(filters.delivery_date);
            let end = new Date(filters.delivery_date);
            end.setHours(23,59,59);
            
            query = query.where('delivered_date', '>=', start.getTime());
            query = query.where('delivered_date', '<=', end.getTime());              
          }

          // deliverer 
          if (this.isAdmin && filters.deliverer && filters.deliverer != 'All') query = query.where("deliverer_id", "==", filters.deliverer);
          else if (!this.isAdmin) {
            query = query.where("deliverer_id", "==", this.authService.uid());
          }

          // truck
          if (filters.truck && filters.truck != 'All') query = query.where('truck', '==', filters.truck);

        } else {
          // defaults
          query = query.orderBy('delivered_date', 'desc');
          
          // current date
          const start = new Date();
          start.setHours(0, 0, 0, 0);
          let end = new Date();
          end.setHours(23, 59, 59);
          
          query = query.where('delivered_date', '>=', start.getTime());
          query = query.where('delivered_date', '<=', end.getTime());
          
          // show only assigned deliveries
          if (!this.isAdmin) query = query.where("deliverer_id", "==", this.authService.uid());
        }
          
        return query;  
      });
      
      this.ordersSubscription = ordersRef.valueChanges()
      .subscribe(docs => {
        this.orders = docs;
        this.hasLoaded = true;
      });     
      
    } catch (error) {
      console.error(error);
      this.hasLoaded = false;
    }
  }

  async add() {
    this.navCtrl.push(OrderDetailPage)
  }

  async openDetail(order) {  
    this.navCtrl.push(OrderDetailPage, {order: order})
  }

  filters() {
    this.navCtrl.push(OrderFilterPage).then(() => this.ordersSubscription.unsubscribe());
  }

  viewLocation(order, event) {
    this.navCtrl.push(LocationPage, {location: order.location, title: order.customer}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });

    event.preventDefault();
    event.stopPropagation();
  }
}
