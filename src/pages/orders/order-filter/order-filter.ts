import { Component } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "../../../services/auth-service";
import { Events } from "ionic-angular";

@Component({
    selector: 'page-order-filter',
    templateUrl: 'order-filter.html'
})
export class OrderFilterPage {
    filter: any = {};
    deliverers: any[] = [];
    trucks: string[] = [];
    isAdmin: boolean;

    constructor(private storage: Storage, private db: AngularFirestore, private authService: AuthService, private events: Events) {
        
    }

    async ngOnInit() {
        const current_user = await this.authService.profile();
        
        const filter = await this.storage.get('order_filters');
        
        if (filter) {
            this.filter = JSON.parse(filter);
        } else {
            this.filter.newest_to_oldest = true;
            this.filter.show_all_dates = true;            
            this.filter.deliverer = current_user.user_type == "deliverer" ? this.authService.uid() : 'All';
            this.filter.truck = 'All'
        }

        this.filter.delivery_date = new Date().toISOString();

        this.isAdmin = await this.authService.isAdmin();
        if (this.isAdmin) {
            this.db.collection('profiles', ref => ref.where('user_type', '==', 'deliverer').orderBy('name', 'asc')).get().toPromise().then(docs => {
                docs.forEach(doc => {
                    this.deliverers.push({
                        id: doc.id,
                        ...doc.data()
                    })
                })
            })

            this.db.collection('trucks').get().toPromise().then(docs => {
                docs.forEach(doc => {
                  this.trucks.push(doc.data().plate_no);
                });
            });
        }
    }

    ionViewWillLeave() {
        this.save();
    }

    async save() {
        if (this.filter.delivery_date) {
            let date = new Date(this.filter.delivery_date);

            this.filter.delivery_date = date.toISOString().split('T')[0];            
        }
        
        await this.storage.set('order_filters', JSON.stringify(this.filter));

        this.events.publish('order_filter_changed');
    }
}