import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, DateTime } from 'ionic-angular';
import { OrderService } from '../../../services/order-service';
import { IOrder } from '../../../app/app.models';
import { AlertService } from '../../../services/alert-service';
import { CameraService } from '../../../services/camera-service';
import { AngularFireStorage } from '@angular/fire/storage';
import { firestore } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { Geolocation } from "@ionic-native/geolocation";
import { LocationPage } from '../../location/location';
import { PhotosPage } from '../../photos/photos';
import { AuthService } from '../../../services/auth-service';
import { Storage } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CustomerSearchPage } from '../../admin/customers/customer-search/customer-search';

@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {
  progress: any;  
  order: IOrder;
  trucks: string[];
  canViewLocation: boolean;
  photosToUpload: any[] = [];

  constructor(public navCtrl: NavController, 
              private navParams: NavParams, 
              private actionSheetCtrl: ActionSheetController,
              private orderService: OrderService,
              private cameraService: CameraService,
              private afStorage: AngularFireStorage,
              private authService: AuthService,
              private db: AngularFirestore,
              private geolocation: Geolocation,
              private localStorage: Storage,
              private modal: ModalController,
              private photoViewer: PhotoViewer,
              private alertService: AlertService) {
    
    this.order = this.navParams.get('order') ? Object.assign({}, this.navParams.get('order') ) : {};  
    
    
  }

  async ionViewDidLoad() {
  
    const isAdmin = await this.authService.isAdmin();
    const isAdminStaff = await this.authService.isAdminStaff();
    this.canViewLocation = isAdmin || isAdminStaff;

    this.db.collection('trucks').get().toPromise().then(docs => {
      this.trucks = [];

      docs.forEach(doc => {
        this.trucks.push(doc.data().plate_no);
      });
    });

    this.getPhotosFromLocalStorage();
  }

  ionViewWillLeave() {
    this.uploadPhotos();
  }

  async save() {
    // TODO: implement form group builder
    const loading = this.alertService.loading('Saving...');

    try {
      if (!this.order.customer || !this.order.customer.trim()) return;

      loading.present();
     
      const _order: IOrder = {
        id: this.order.id,
        delivered_date: new Date().getTime(),
        customer: this.order.customer,
        truck: this.order.truck || '',
        remarks: this.order.remarks || '',
        paid: this.order.paid || false
      };
      
      if (!_order.id) this.order.id = await this.orderService.addOrder(_order);
      else {
        await this.orderService.updateOrder(_order)
        this.navCtrl.pop();
      }

    } catch (error) {
      console.error(error)
      this.alertService.toast('Oops! Unable to connect server. Please check internet connection and try again.')
    } finally {
      if (loading) loading.dismiss();
    }
  }

  addPhoto() {
    // this.actionSheetCtrl.create({
    //   title: 'Add photo using...',
    //   buttons: [
    //     {
    //       text: 'Camera',
    //       handler: () => {
    //         this.takePhoto(true);
    //       }
    //     },
    //     {
    //       text: 'Photo Gallery',
    //       handler: () => {
    //         this.takePhoto(false);
    //       }
    //     }
    //   ]
    // }).present();

    this.takePhoto(true);
  }

  async takePhoto(useCamera: boolean) {
    
    try {

      const today = new Date();
      today.setHours(0,0,0,0);

      const photo = await this.cameraService.takePicture(useCamera);
      
      this.photosToUpload.push({
        date: today.getTime(),
        url: photo,
        progress: 0
      });

      this.savePhotosToLocalStorage();

    } catch (error) {      
      this.alertService.toast('Error accessing camera. Make sure this APP has camera permission in Settings -> App Permissions.');
    }
  }

  async uploadPhotos() {    
    const photos = this.photosToUpload.filter(x => x.progress !== 100);
    
    if (photos.length !== 0 && (!this.order.location || this.order.location.latitude !== 0)) {
      const loader = await this.alertService.loading('Uploading photos...');
      loader.present(); 
      try {
        const location = await this.getCurrentLocation();
      
        await this.orderService.updateOrder({
          id: this.order.id,
          delivered_date: new Date().getTime(),
          location: location
        });
        
        this.order.location = location;
        
      } catch (error) {
        
      } finally {
        if (loader) loader.dismiss();
      }
    }

    photos.forEach(photo => this.uploadPhoto(photo));
  }

  async uploadPhoto(photo: any) {
    const filePath = `orders/${this.order.id}/` + new Date().getTime() + '.jpeg';
    const fileRef = this.afStorage.ref(filePath);
    const task = fileRef.putString(photo.url, 'data_url');
    
    task.percentageChanges().subscribe(progress => photo.progress = progress);
    
    task.then(() => {
      
      fileRef.getDownloadURL().toPromise().then(async (url) => {
        this.order.photos = this.order.photos || [];
        this.order.photos.push(url);
        
        this.orderService.updateOrder({
          id: this.order.id,
          photos: firestore.FieldValue.arrayUnion(url),
          status: 'delivered'
        });
        
        const id = this.db.createId();
        this.db.collection('photos').doc(id).set({
          id: id,
          source: 'order',
          ref_id: this.order.id,
          date_time: new Date().getTime(),
          url: url,
          backed_up: false
        });

        this.savePhotosToLocalStorage();
      });
    }).catch(error => {
      this.alertService.toast('Oops! An error occured while uploading image.')
      console.log(JSON.stringify(error))
    })
  }

  removePhoto(index) {
    this.photosToUpload.splice(index, 1);
    this.savePhotosToLocalStorage();
  }

  viewPhoto(photo) {
      this.photoViewer.show(photo, '');
  }

  async getCurrentLocation(){
    try {
      var options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };
        
      const position = await this.geolocation.getCurrentPosition(options);  
  
      return {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }
    } catch (error) {
      this.alertService.toast('Error getting location. Make sure Global 168 has LOCATION permission granted in Settings -> App Permissions.');
      return {
        latitude: 0,
        longitude: 0
      };
    }
  }

  showLocation() {
    this.navCtrl.push(LocationPage, { location: this.order.location }, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  showPhotos() {
    this.navCtrl.push(PhotosPage, { photos: this.order.photos }, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  showPhotosToUpload() {
    this.navCtrl.push(PhotosPage, { photos: this.photosToUpload, title: 'Pending photos', pending: true }, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  async savePhotosToLocalStorage() {
    this.localStorage.set('photos-' + this.order.id, this.photosToUpload);
  }

  async getPhotosFromLocalStorage() {
    const photos: any[] = await this.localStorage.get('photos-' + this.order.id);
    if (photos) this.photosToUpload = photos.filter(x => x.progress !== 100);
  }

  pendingPhotos(): boolean {
    return this.photosToUpload.filter(x => x.progress !== 100).length !== 0;
  }

  searchCustomer() {
    const modal = this.modal.create(CustomerSearchPage);

    modal.onDidDismiss(data => {
      if (data) this.order.customer = data;
    });

    modal.present();
  }
}
