import { Component, ChangeDetectorRef } from "@angular/core";
import { Storage } from "@ionic/storage";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "../../../services/auth-service";
import { Events } from "ionic-angular";

@Component({
    selector: 'page-collection-filter',
    templateUrl: 'collection-filter.html'
})
export class CollectionFilterPage {
    filter: any = {};
    collectors: any[] = [];
    isAdmin: boolean;

    constructor(private storage: Storage, private db: AngularFirestore, private authService: AuthService, private events: Events, private cdr: ChangeDetectorRef) {
        
    }

    async ngOnInit() {
        const current_user = await this.authService.profile();
        
        const filter = await this.storage.get('collection_filters');
        
        if (filter) {
            this.filter = JSON.parse(filter);
        } else {
            this.filter.newest_to_oldest = true;
            this.filter.show_all_dates = true;            
            this.filter.collector = current_user.user_type == "collector" ? this.authService.uid() : 'All';
        }

        this.filter.collection_date = new Date().toISOString();

        this.isAdmin = await this.authService.isAdmin();
        if (this.isAdmin) {
            this.db.collection('profiles', ref => ref.where('user_type', '==', 'collector').orderBy('name', 'asc')).get().toPromise().then(docs => {
                this.collectors = [];

                docs.forEach(doc => {
                    this.collectors.push({
                        id: doc.id,
                        ...doc.data()
                    })
                })

                this.cdr.detectChanges();
            });
        }
    }

    ionViewWillLeave() {
        this.save();
    }

    async save() {
        if (this.filter.collection_date) {
            let date = new Date(this.filter.collection_date);

            this.filter.collection_date = date.toISOString().split('T')[0];            
        }
        
        await this.storage.set('collection_filters', JSON.stringify(this.filter));

        this.events.publish('collection_filter_changed');
    }
}