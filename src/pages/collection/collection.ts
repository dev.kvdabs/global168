import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "../../services/auth-service";
import { Subscription } from "rxjs";
import { Storage } from "@ionic/storage";
import { Events, NavController } from "ionic-angular";
import { CollectionFilterPage } from "./filter/collection-filter";
import { CollectionDetailPage } from "./collection-detail/collection-detail";
import { LocationPage } from "../location/location";

@Component({
    selector: 'page-collections',
    templateUrl: 'collection.html'
})

export class CollectionPage {
    hasLoaded: boolean;
    isAdmin: boolean;
    applyFilter: boolean;

    subscription: Subscription;
    collections: any[];


    constructor(private db: AngularFirestore, 
                private nav: NavController,
                private storage: Storage,
                private events: Events,
                private authService: AuthService) {

        this.events.subscribe('collection_filter_changed', () => {
          this.applyFilter = true;
          this.getCollections();
        });   
    }

    async ionViewDidLoad() {
        this.isAdmin = await this.authService.isAdmin() || await this.authService.isAdminStaff();
    
        this.getCollections();
    }

    async getCollections() {
        try {    
            this.hasLoaded = false;
            
            const _filters = await this.storage.get('collection_filters');
            const filters = _filters ? JSON.parse(_filters) : null;

            const ref = this.db.collection('collections', ref => {
              let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
              
              if (this.applyFilter && filters) {
                query = query.orderBy('collection_date', filters.newest_to_oldest ? 'desc' : 'asc');
                
                if (!filters.show_all_dates) {
                  const start = new Date(filters.collection_date);
                  let end = new Date(filters.collection_date);
                  end.setHours(23,59,59);
                  
                  query = query.where('collection_date', '>=', start.getTime());
                  query = query.where('collection_date', '<=', end.getTime());              
                }
      
                // collector 
                if (this.isAdmin && filters.collector && filters.collector != 'All') query = query.where("collector_id", "==", filters.collector);
                else if (!this.isAdmin) {
                  query = query.where("collector_id", "==", this.authService.uid());
                }
      
              } else {
                // defaults
                query = query.orderBy('collection_date', 'desc');
                // current date
                const start = new Date();
                start.setHours(0, 0, 0, 0);
                let end = new Date();
                end.setHours(23, 59, 59);
                
                query = query.where('collection_date', '>=', start.getTime());
                query = query.where('collection_date', '<=', end.getTime());
                
                // show only assigned collectors
                if (!this.isAdmin) query = query.where("collector_id", "==", this.authService.uid());
              }
                
              return query;  
            });
            
            this.subscription = ref.valueChanges()
            .subscribe(docs => {
              this.collections = docs;
              console.log(this.collections)
              this.hasLoaded = true;
            });
            
        } catch (error) {
            console.error(error);
            this.hasLoaded = false;
        }
    }

    filters() {
        this.nav.push(CollectionFilterPage, {}, {
            animate: true,
            animation: 'transition',
            direction: 'forward'
          });
    }

    async add() {
        this.nav.push(CollectionDetailPage)
    }

    async openDetail(collection) {  
        this.nav.push(CollectionDetailPage, {collection: collection})
    }

    viewLocation(collection, event) {
        this.nav.push(LocationPage, {location: collection.location, title: collection.customer}, {
          animate: true,
          animation: 'transition',
          direction: 'forward'
        });
    
        event.preventDefault();
        event.stopPropagation();
    }
}