import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ActionSheetController } from 'ionic-angular';
import { ICollection } from '../../../app/app.models';
import { AlertService } from '../../../services/alert-service';
import { CameraService } from '../../../services/camera-service';
import { AngularFireStorage } from '@angular/fire/storage';
import { firestore } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { Geolocation } from "@ionic-native/geolocation";
import { LocationPage } from '../../location/location';
import { PhotosPage } from '../../photos/photos';
import { AuthService } from '../../../services/auth-service';
import { CollectionService } from '../../../services/collection-service';
import { CustomerSearchPage } from '../../admin/customers/customer-search/customer-search';
import { Storage } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'page-collection-detail',
  templateUrl: 'collection-detail.html',
})
export class CollectionDetailPage {
  progress: any;  
  collection: ICollection;
  canViewLocation: boolean;
  photoToUpload: string;
  photosToUpload: any[] = [];

  constructor(public navCtrl: NavController, 
              private navParams: NavParams,
              private actionSheetCtrl: ActionSheetController,
              private collectionService: CollectionService,
              private cameraService: CameraService,
              private afStorage: AngularFireStorage,
              private authService: AuthService,
              private db: AngularFirestore,
              private geolocation: Geolocation,
              private localStorage: Storage,
              private photoViewer: PhotoViewer,
              private modal: ModalController,
              private alertService: AlertService) {
    
    this.collection = this.navParams.get('collection') ? Object.assign({}, this.navParams.get('collection') ) : {};  
    
    
  }

  async ionViewDidLoad() {
  
    const isAdmin = await this.authService.isAdmin();
    const isAdminStaff = await this.authService.isAdminStaff();
    this.canViewLocation = isAdmin || isAdminStaff;  
    
    this.getPhotosFromLocalStorage();
  }

  ionViewWillLeave() {
    this.uploadPhotos();
  }

  async save() {
    // TODO: implement form group builder
    const loading = this.alertService.loading('Saving...');

    try {
      if (!this.collection.customer || !this.collection.customer.trim()) return;

      loading.present();
     
      const _collection: ICollection = {
        id: this.collection.id,
        customer: this.collection.customer,
        remarks: this.collection.remarks || ''
      };
      
      if (!_collection.id) {
        _collection.collection_date = new Date().getTime();

        const location = await this.getCurrentLocation();
        _collection['location'] = location;
        this.collection.id = await this.collectionService.add(_collection)
        this.collection.location = location;
        
      } else {
        await this.collectionService.update(_collection)
        this.navCtrl.pop();
      }

    } catch (error) {
      console.error(error)
      this.alertService.toast('Oops! Unable to connect server. Please check internet connection and try again.')
    } finally {
      if (loading) loading.dismiss();
    }
  }

  addPhoto() {
    // this.actionSheetCtrl.create({
    //   title: 'Add photo using...',
    //   buttons: [
    //     {
    //       text: 'Camera',
    //       handler: () => {
    //         this.takePhoto(true);
    //       }
    //     },
    //     {
    //       text: 'Photo Gallery',
    //       handler: () => {
    //         this.takePhoto(false);
    //       }
    //     }
    //   ]
    // }).present();

    this.takePhoto(true);
  }

  async takePhoto(useCamera: boolean) {
    
    try {

      const today = new Date();
      today.setHours(0,0,0,0);

      const photo = await this.cameraService.takePicture(useCamera);
      
      this.photosToUpload.push({
        date: today.getTime(),
        url: photo,
        progress: 0
      });

      this.savePhotosToLocalStorage();

    } catch (error) {
      this.alertService.toast('Error accessing camera. Make sure Global 168 has CAMERA permission in Settings -> App Permissions.');
    }
  }

  async getCurrentLocation(){
    try {
      var options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };
        
      const position = await this.geolocation.getCurrentPosition(options);  
  
      return {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }
    } catch (error) {
      this.alertService.toast('Error getting location. Make sure Global 168 has LOCATION permission granted in Settings -> App Permissions.');
      return {
        latitude: 0,
        longitude: 0
      };
    }
  }

  removePhoto(index) {
    this.photosToUpload.splice(index, 1);
    this.savePhotosToLocalStorage();
  }

  viewPhoto(photo) {
    this.photoViewer.show(photo, '');
  }

  showLocation() {
    this.navCtrl.push(LocationPage, { location: this.collection.location }, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  showPhotos() {
    this.navCtrl.push(PhotosPage, { photos: this.collection.photos }, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  async uploadPhotos() {    
    const photos = this.photosToUpload.filter(x => x.progress !== 100);
    
    if (photos.length !== 0 && (!this.collection.location || this.collection.location.latitude !== 0)) {
      const loader = await this.alertService.loading('Uploading photos...');
      loader.present(); 
      try {
        const location = await this.getCurrentLocation();
      
        await this.collectionService.update({
          id: this.collection.id,
          delivered_date: new Date().getTime(),
          location: location
        });
        
        this.collection.location = location;
        
      } catch (error) {
        
      } finally {
        if (loader) loader.dismiss();
      }
    }

    photos.forEach(photo => this.uploadPhoto(photo));
  }

  async uploadPhoto(photo: any) {
    const filePath = `collections/${this.collection.id}/` + new Date().getTime() + '.jpeg';
    const fileRef = this.afStorage.ref(filePath);
    const task = fileRef.putString(photo.url, 'data_url');
    
    task.percentageChanges().subscribe(progress => photo.progress = progress);
    
    task.then(() => {
      
      fileRef.getDownloadURL().toPromise().then(async (url) => {
        this.collection.photos = this.collection.photos || [];
        this.collection.photos.push(url);
        
        this.collectionService.update({
          id: this.collection.id,
          photos: firestore.FieldValue.arrayUnion(url),
          status: 'delivered'
        });
        
        const id = this.db.createId();
        this.db.collection('photos').doc(id).set({
          id: id,
          source: 'collection',
          ref_id: this.collection.id,
          date_time: new Date().getTime(),
          url: url,
          backed_up: false
        });

        this.savePhotosToLocalStorage();
      });
    }).catch(error => {
      this.alertService.toast('Oops! An error occured while uploading image.')
      console.log(JSON.stringify(error))
    })
  }

  async savePhotosToLocalStorage() {
    this.localStorage.set('photos-' + this.collection.id, this.photosToUpload);
  }

  async getPhotosFromLocalStorage() {
    const photos: any[] = await this.localStorage.get('photos-' + this.collection.id);
    if (photos) this.photosToUpload = photos.filter(x => x.progress !== 100);
  }

  pendingPhotos(): boolean {
    return this.photosToUpload.filter(x => x.progress !== 100).length !== 0;
  }

  searchCustomer() {
    const modal = this.modal.create(CustomerSearchPage);

    modal.onDidDismiss(data => {
      if (data) this.collection.customer = data;
    });

    modal.present();
  }
}
