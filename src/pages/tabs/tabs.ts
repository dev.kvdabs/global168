import { Component, ChangeDetectorRef } from '@angular/core';
import { VisitsPage } from '../visits/visits';
import { OrdersPage } from '../orders/orders';
import { SettingsPage } from '../settings/settings';
import { AdminPage } from '../admin/admin';
import { AuthService } from '../../services/auth-service';
import { NavController } from 'ionic-angular';
import { CollectionPage } from '../collection/collection';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  ordersTab = OrdersPage;
  visitsTab = VisitsPage;
  adminTab = AdminPage;
  settingsTab = SettingsPage;
  collectionsTab = CollectionPage;

  tabs = [];

  constructor(private nav: NavController, private authService: AuthService, private cdr: ChangeDetectorRef) {
    
  }

  async ngOnInit() {
    
    this.tabs.push({
      root: this.settingsTab,
      title: 'Home',
      icon: 'ios-home-outline'
    });

    if (await this.authService.isAdmin()) {
      const tabs = [
        {
          root: this.ordersTab,
          title: 'Deliveries',
          icon: 'ios-paper-plane'
        },
        {
          root: this.collectionsTab,
          title: 'Collections',
          icon: 'ios-cash'
        },
        {
          root: this.visitsTab,
          title: 'Visits',
          icon: 'ios-time'
        },
        {
          root: this.adminTab,
          title: 'Admin',
          icon: 'ios-finger-print'
        }
      ];

      this.tabs.push(...tabs)
    } 
    else if (await this.authService.isAdminStaff()) {
      const tabs = [
        {
          root: this.ordersTab,
          title: 'Deliveries',
          icon: 'ios-paper-plane'
        },
        {
          root: this.visitsTab,
          title: 'Visits',
          icon: 'ios-time'
        }
      ];

      this.tabs.push(...tabs)
    }
    else if (await this.authService.isAgent()) {
      const tabs = [        
        {
          root: this.visitsTab,
          title: 'Visits',
          icon: 'ios-time'
        }
      ];

      this.tabs.push(...tabs)

    } else if (await this.authService.isDeliverer()) {
      const tabs = [
        {
          root: this.ordersTab,
          title: 'Deliveries',
          icon: 'ios-paper-plane'
        },
        {
          root: this.collectionsTab,
          title: 'Collections',
          icon: 'ios-cash'
        }
      ];

      this.tabs.push(...tabs)
    } else if (await this.authService.isCollector()) {
      const tabs = [
        {
          root: this.collectionsTab,
          title: 'Collections',
          icon: 'ios-cash'
        }
      ];

      this.tabs.push(...tabs)
    }

    this.cdr.detectChanges();
  }  
}
