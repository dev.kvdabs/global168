import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { VisitDetailPage } from './visit-detail/visit-detail';
import { AuthService } from '../../services/auth-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'page-visits',
  templateUrl: 'visits.html',
})
export class VisitsPage {
  visits: any[] = [];
  isAdmin: boolean;
  agents: any[];
  filter_agent: string;
  subscription: Subscription;
  hasLoaded: boolean;

  constructor(public navCtrl: NavController, public db: AngularFirestore, private authService: AuthService) {
    
  }

  async ionViewDidLoad(agentId) {
    this.isAdmin = await this.authService.isAdmin() || await this.authService.isAdminStaff();

    this.getVisits();    

    if (this.isAdmin) {
      this.db.collection('profiles', ref => ref.where("user_type", "==", "sales_agent").orderBy("name", "asc")).get().toPromise().then(docs => {
        this.agents = [];
        docs.forEach(doc => {
          this.agents.push({
            id: doc.id,
            name: doc.data().name
          })
        })
      })
    }
  }

  getVisits(agentId?: string) {
    try {
      this.hasLoaded = false;

      const ref = this.db.collection('visits', ref => {
        let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
  
        query = query.orderBy("in", "desc");
  
        if (!this.isAdmin) {
          query = query.where("agent_id", "==", this.authService.uid());
        }
  
        if (agentId && agentId != "All") query = query.where("agent_id", "==", agentId);
        else {
          query = query.limit(100);
          
          // let start = new Date()
          // start.setHours(0,0,0);
  
          // let end = new Date();
          // end.setHours(23,59,59);
                  
          // query = query.where("in", ">=", start.getTime());
          // query = query.where("in", "<=", end.getTime());
        }
  
        return query;
      });
  
      this.subscription = ref.valueChanges().subscribe(docs => {
        this.visits = [];
        this.visits = docs;
        this.hasLoaded = true;
      });
    } catch (error) {
      this.hasLoaded = false;
    }
  }

  add() {
    this.navCtrl.push(VisitDetailPage);
  }

  openDetail(visit) {
    this.navCtrl.push(VisitDetailPage, {visit: visit});
  }

  applyFilter() {
    this.subscription.unsubscribe();
    this.getVisits(this.filter_agent);
  }
}
