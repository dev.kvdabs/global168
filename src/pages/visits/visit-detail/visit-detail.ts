import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ActionSheetController } from 'ionic-angular';
import { IVisit } from '../../../app/app.models';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../../services/auth-service';
import { AlertService } from '../../../services/alert-service';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CameraService } from '../../../services/camera-service';
import { AngularFireStorage } from '@angular/fire/storage';
import { firestore } from 'firebase';
import { CustomerSearchPage } from '../../admin/customers/customer-search/customer-search';

@Component({
  selector: 'page-visit-detail',
  templateUrl: 'visit-detail.html',
})
export class VisitDetailPage {
  visit: IVisit;
  uploadProgress: any; 
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private actionSheetCtrl: ActionSheetController,
              private db: AngularFirestore, 
              private authService: AuthService, 
              private photoViewer: PhotoViewer,
              private cameraService: CameraService,
              private afStorage: AngularFireStorage,
              private modal: ModalController,
              private alertService: AlertService) {

    this.visit = this.navParams.get('visit') ? Object.assign({}, this.navParams.get('visit') ) : {}
  }

  ionViewDidLoad() {
    
  }

  async save(clockout: boolean) {
    const loading = this.alertService.loading('Saving...');
    loading.present();

    try {
      if (!this.visit.id) {
        const id = this.db.createId();
        const add = {
          id: id,
          in: new Date().getTime(),
          customer: this.visit.customer,
          contact_person: this.visit.contact_person || '',
          remarks: this.visit.remarks || '',
          agent_id: this.authService.uid(),
          agent_name: (await this.authService.profile()).name
        };
  
        await this.db.collection('visits').doc(id).set(add);
        
        this.visit = add;
      } else {
  
        let update: any = {
          customer: this.visit.customer,          
          remarks: this.visit.remarks || '',
        }

        if (this.visit.contact_person) update['contact_person'] = this.visit.contact_person;

        if (clockout) update.out = new Date().getTime();

        await this.db.collection('visits').doc(this.visit.id).update(update);

        this.navCtrl.pop();
      }
      
    } catch (error) {
      console.error(error)
      this.alertService.toast('Oops! Unable to connect server. Please check internet connection and try again.')
    } finally {
      if (loading) loading.dismiss();
    }
  }

  out() {
    this.save(true);
  }

  addPhoto() {
    // this.actionSheetCtrl.create({
    //   title: 'Add photo using...',
    //   buttons: [
    //     {
    //       text: 'Camera',
    //       handler: () => {
    //         this.takePhoto(true);
    //       }
    //     },
    //     {
    //       text: 'Photo Gallery',
    //       handler: () => {
    //         this.takePhoto(false);
    //       }
    //     }
    //   ]
    // }).present();

    this.takePhoto(true);
  }

  async takePhoto(useCamera: boolean) {
    try {
      const photo = await this.cameraService.takePicture(useCamera);
      
      const filePath = `visits/${this.visit.id}/` + new Date().getTime() + '.jpeg';
      const fileRef = this.afStorage.ref(filePath);
      const task = fileRef.putString(photo, 'data_url');
      
      this.uploadProgress = task.percentageChanges();

      task.then(() => {
        this.uploadProgress = null;

        fileRef.getDownloadURL().toPromise().then((url) => {
          this.visit.photos = this.visit.photos || [];
          this.visit.photos.push(url);
          
          this.db.collection('visits').doc(this.visit.id).update({
            photos: firestore.FieldValue.arrayUnion(url),
          });
          
        });
      }).catch(error => {
        this.alertService.toast('Oops! An error occured while uploading image.')
        console.log(JSON.stringify(error))
      })

    } catch (error) {
      alert(JSON.stringify(error))
    }
  }

  viewPhoto(photo) {
    this.photoViewer.show(photo, '');
  }

  searchCustomer() {
    const modal = this.modal.create(CustomerSearchPage);

    modal.onDidDismiss(data => {
      if (data) this.visit.customer = data;
    });

    modal.present();
  }
}
