import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { IProfile } from "../../../app/app.models";
import { NavController } from "ionic-angular";
import { UserDetailPage } from "./user-detail/user-detail";

@Component({
    selector: 'page-users',
    templateUrl: 'users.html'
})

export class UsersPage {
    users: any[] = [];

    constructor(private db: AngularFirestore, private nav: NavController) {

    }

    ionViewDidLoad() {
        this.db.collection('profiles', ref => ref.orderBy('name', 'asc')).valueChanges().subscribe(docs => {
            this.users = [];
            docs.forEach(doc => this.users.push(doc))
        })
    }

    openDetail(user: IProfile) {
        this.nav.push(UserDetailPage, {user:user});
    }

    add() {
        this.nav.push(UserDetailPage)
    }
}