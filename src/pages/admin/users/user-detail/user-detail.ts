import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { IProfile } from '../../../../app/app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertService } from '../../../../services/alert-service';
import { AuthService } from '../../../../services/auth-service';

@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {
  user: IProfile;
  form: FormGroup;

  constructor(public navCtrl: NavController, 
              public params: NavParams, 
              private formBuilder: FormBuilder, 
              private db: AngularFirestore, 
              private afAuth: AngularFireAuth,
              private alertService: AlertService) {
    this.user = this.params.get('user') || new IProfile();
    
    this.form = this.formBuilder.group({
      id: [this.user.id],
      name: [this.user.name, Validators.required],
      username: [this.user.username, Validators.required],
      password: [this.user.password],
      user_type: [this.user.user_type, Validators.required],
      active: [this.user.id ? this.user.active : true, Validators.required],
    });
  }

  async save() {
    const loading = this.alertService.loading('Saving...');

    try {
      if (this.form.valid) {
        loading.present();

        if (!this.user.id && (!this.form.controls.password || this.form.controls.password.value.length < 6)) {
          return this.alertService.toast('Password must be atleast 6 characters');
        }
  
        let user: IProfile = this.form.value;
  
        if (user.id) {
          delete user.password;
          await this.db.collection('profiles').doc(user.id).update(user);
          this.navCtrl.pop();
        } else {        
          this.createUser(user);
        }
      } else {
        this.alertService.toast('Please fill all fields');
      }    
    } catch (error) {
      
    } finally {
      if (loading) loading.dismiss();
    }
  }

  async createUser(user: IProfile) {
    try {
      const credential = await this.afAuth.auth.createUserWithEmailAndPassword(user.username + '@global168.com', user.password);
      const id = credential.user.uid;
      user.id = id;

      await this.db.collection('profiles').doc(id).set(user);
      
      this.alertService.toast('User created and logged in.');

    } catch (error) {
      if (error.code == 'auth/email-already-in-use') return this.alertService.toast('Oops! username already exist.');
      this.alertService.toast('Oops! An error occured while creating user account');
      console.log(error)
    }
  }

}
