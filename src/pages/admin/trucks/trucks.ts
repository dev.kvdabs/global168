import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'page-trucks',
  templateUrl: 'trucks.html',
})
export class TrucksPage {
  trucks: any;
  newTruck: string = '';

  constructor(public navCtrl: NavController, public db: AngularFirestore) {
  }

  ionViewDidLoad() {
    this.trucks = this.db.collection('trucks').valueChanges();
  }

  async save() {
    if (this.newTruck.trim()) {

      try {
        const id = this.db.createId();
        await this.db.collection('trucks').doc(id).set({
          id: id,
          plate_no: this.newTruck.toUpperCase()
        });

        this.newTruck = '';
      } catch (error) {
        console.log(error)
      }
      
    }
  }

  async remove(truck: any) {
    try {
      await this.db.collection('trucks').doc(truck.id).delete();
    } catch (error) {
      
    }
  }
}
