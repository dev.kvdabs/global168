import { Component } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { NavController, NavParams } from "ionic-angular";
import { ICustomer } from "../../../app/app.models";
import { CustomerDetailPage } from "./customer-detail/customer-detail";

@Component({
    selector: 'page-customers',
    templateUrl: 'customers.html'
})

export class CustomersPage {
    customers = [];

    constructor(private db: AngularFirestore, private nav: NavController) {
    }

    ionViewDidLoad() {
        const ref = this.db.collection('customers', ref => {
            let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      
            query = query.orderBy("name", "asc");
            
            return query;
        });

        ref.valueChanges().subscribe(docs => {
            console.log(docs)
            this.customers = [];
            docs.forEach(doc => this.customers.push(doc))
        })
    }

    openDetail(customer: ICustomer) {
        this.nav.push(CustomerDetailPage, {customer});
    }

    add() {
        this.nav.push(CustomerDetailPage)
    }
}