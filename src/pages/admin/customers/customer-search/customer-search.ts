import { Component, ChangeDetectorRef } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ViewController } from "ionic-angular";
import { ICustomer } from "../../../../app/app.models";

@Component({
    selector: 'page-customers',
    templateUrl: 'customer-search.html'
})

export class CustomerSearchPage {
    customers = [];
    results = [];

    constructor(private db: AngularFirestore, private viewCtrl: ViewController, private cdr: ChangeDetectorRef) {
    }

    ionViewDidLoad() {
        this.db.collection('customers', ref => ref.orderBy('name', 'asc')).get().toPromise().then(docs => {
            docs.forEach(doc => {
              this.customers.push(doc.data());
            });
        });
    }

    filter(ev: any) {
        const val = ev.target.value;

        if (val && val.trim() != '') {           
            this.results = this.customers.filter((item) => {
              return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.results = [];
        }

        this.cdr.detectChanges();
    }

    select(customer: ICustomer) {
        this.viewCtrl.dismiss(customer.name);
    }

    close() {
        this.viewCtrl.dismiss();
    }
}