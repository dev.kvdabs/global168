import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "../../../../services/auth-service";
import { AlertService } from "../../../../services/alert-service";
import { ICustomer } from "../../../../app/app.models";

@Component({
    selector: 'page-customer-detail',
    templateUrl: 'customer-detail.html'
})

export class CustomerDetailPage {
    customer: ICustomer;

    constructor(public navCtrl: NavController, 
        public navParams: NavParams, 
        private db: AngularFirestore, 
        private authService: AuthService,
        private alertService: AlertService) {

        this.customer = this.navParams.get('customer') ? Object.assign({}, this.navParams.get('customer') ) : {}
    }

    ionViewDidLoad() {

    }

    async save() {
        const loading = this.alertService.loading('Saving...');
        loading.present();

        try {
            if (!this.customer.id) {
                const id = this.db.createId();
                const add = {
                    id: id,
                    name: this.customer.name,
                    contact_person: this.customer.contact_person,
                    address: this.customer.address
                };

                await this.db.collection('customers').doc(id).set(add);
            } else {

                let update: any = {
                    name: this.customer.name,
                    contact_person: this.customer.contact_person || '',
                    address: this.customer.address || ''
                }

                await this.db.collection('customers').doc(this.customer.id).update(update);
            }

            this.navCtrl.pop();
        } catch (error) {
            console.error(error)
            this.alertService.toast('Oops! Unable to connect server. Please check internet connection and try again.')
        } finally {
            if (loading) loading.dismiss();
        }
    }
}