import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AFStorageService } from '../../../services/afStorageService';
import * as moment from 'moment';
import { AlertService } from '../../../services/alert-service';
import { File } from "@ionic-native/file";

@Component({
  selector: 'page-backup',
  templateUrl: 'backup.html',
})
export class BackupPage {
  photos: any[];
  done = 0;
  progress: number = 0;

  constructor(public navCtrl: NavController, public db: AngularFirestore, private afStorage: AFStorageService, private file: File, private alertService: AlertService) {
  }

  async ionViewDidLoad() {
    this.photos = [];

    const photos = await this.db.collection('photos', ref => ref.where('backed_up', '==', false)).get().toPromise()
    
    photos.docs.forEach(doc => {
      this.photos.push(doc.data());      
    })
  }

  async backup() {
    
    this.photos.forEach(photo => {
      const filename = 'global-' + moment(photo.date_time).format('MM_DD_YYYY') + '-' + Math.floor(Math.random() * 10000000) + '.jpeg';      
      
      this.afStorage.download(photo.url, filename).then(blob => {

        this.file.writeFile(this.file.externalRootDirectory + "/Download", filename, blob, {replace: true}).then(() => {
          this.done++;
          this.progress = Math.floor(this.done/this.photos.length *100)

          this.db.collection('photos').doc(photo.id).update({
            backed_up: true
          });
        })        
        
      });      
    })
  }
}
