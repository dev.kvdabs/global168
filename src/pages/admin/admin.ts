import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UsersPage } from '../admin/users/users';
import { TrucksPage } from './trucks/trucks';
import { BackupPage } from './backup/backup';
import { CustomersPage } from './customers/customers';

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
  }

  users() {
    this.navCtrl.push(UsersPage, {}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  trucks() {
    this.navCtrl.push(TrucksPage, {}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  customers() {
    this.navCtrl.push(CustomersPage, {}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }

  download() {
    this.navCtrl.push(BackupPage, {}, {
      animate: true,
      animation: 'transition',
      direction: 'forward'
    });
  }
}
