import { AuthService } from "../services/auth-service";
import { OrderService } from "../services/order-service";
import { AlertService } from "../services/alert-service";
import { CameraService } from "../services/camera-service";
import { AFStorageService } from "../services/afStorageService";
import { CollectionService } from "../services/collection-service";

export const APP_SERVICES: any[] = [
    AuthService,
    OrderService,
    CollectionService,
    AlertService,
    CameraService,
    AFStorageService
]