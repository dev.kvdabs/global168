import { LoginPage } from "../pages/login/login";
import { VisitsPage } from "../pages/visits/visits";
import { OrdersPage } from "../pages/orders/orders";
import { UsersPage } from "../pages/admin/users/users";
import { OrderDetailPage } from "../pages/orders/order-detail/order-detail";
import { SettingsPage } from "../pages/settings/settings";
import { OrderFilterPage } from "../pages/orders/order-filter/order-filter";
import { AdminPage } from "../pages/admin/admin";
import { UserDetailPage } from "../pages/admin/users/user-detail/user-detail";
import { VisitDetailPage } from "../pages/visits/visit-detail/visit-detail";
import { TrucksPage } from "../pages/admin/trucks/trucks";
import { BackupPage } from "../pages/admin/backup/backup";
import { LocationPage } from "../pages/location/location";
import { PhotosPage } from "../pages/photos/photos";
import { ChangePasswordPage } from "../pages/settings/change-password/change-password";
import { CollectionPage } from "../pages/collection/collection";
import { CollectionFilterPage } from "../pages/collection/filter/collection-filter";
import { CollectionDetailPage } from "../pages/collection/collection-detail/collection-detail";
import { CustomerDetailPage } from "../pages/admin/customers/customer-detail/customer-detail";
import { CustomersPage } from "../pages/admin/customers/customers";
import { CustomerSearchPage } from "../pages/admin/customers/customer-search/customer-search";
import { PhotoPage } from "../pages/photos/photo/photo";

export const APP_PAGES: any[] = [
    LoginPage,
    OrdersPage,
    OrderDetailPage,
    OrderFilterPage,
    VisitsPage,
    VisitDetailPage,
    AdminPage,
    UsersPage,
    UserDetailPage,
    TrucksPage,
    BackupPage,
    SettingsPage,
    LocationPage,
    PhotosPage,
    PhotoPage,
    ChangePasswordPage,
    CollectionPage,
    CollectionDetailPage,
    CollectionFilterPage,
    CustomersPage,
    CustomerDetailPage,
    CustomerSearchPage
]