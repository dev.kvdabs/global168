import { DocumentReference } from "@angular/fire/firestore";

export class IBase {
    id?: string;
}

export class IOrder extends IBase {  
    customer: string;
    address?: string;
    delivered_date?: number;
    deliverer_id?: string;
    deliverer_name?: string;
    truck: string;
    status?: string;
    remarks?: string = '';
    photos?: string[];
    location?: ILocation;
    paid?: boolean;
}

export class IProfile {
    id: string;
    username: string;
    password: string;
    name: string;
    user_type: string;
    active: boolean;
}

export class IVisit extends IBase {
    agent_id: string;
    agent_name: string;
    customer: string;
    contact_person?: string;
    in: number;
    out?: number;
    remarks?: string;
    photos?: string[];
}

export class ICollection extends IBase {  
    customer: string;    
    collection_date?: number;
    collector_id?: string;
    collector_name?: string;
    remarks?: string = '';
    photos?: string[];
    location?: ILocation;
}

export class ILocation {
    latitude: number;
    longitude: number;
}

export class ICustomer {
    id: string;
    name: string;
    contact_person: string;
    address: string;
}