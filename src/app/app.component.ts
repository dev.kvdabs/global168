import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../pages/tabs/tabs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../services/auth-service';
import { AlertService } from '../services/alert-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, 
              splashScreen: SplashScreen, 
              private storage: Storage, 
              private afAuth: AngularFireAuth, 
              private authService: AuthService, 
              private app: App, 
              private alertService: AlertService) {

    platform.ready().then(() => {
      splashScreen.hide();
      
      this.watchAuthState();
      this.deletePhotosFromLocalStorage();
    });
  }

  watchAuthState() {
    const $this = this;

    this.afAuth.auth.onAuthStateChanged(async function(user) {
      if (user) {        
        const profile = await $this.authService.getProfile();
        if (profile.exists) {
          const _profile = profile.data();
          
          if (!_profile || !_profile.active) {
            $this.alertService.toast('User is inactive');
            return $this.app.getRootNav().setRoot(LoginPage, { showForm: true });
          }

          await $this.storage.set('current_firebase_user', JSON.stringify({
            profile: _profile
          }));

          $this.app.getRootNav().setRoot(TabsPage);
        } else {
          $this.alertService.toast('User not found');
          return $this.app.getRootNav().setRoot(LoginPage, { showForm: true });
        }
        
      } else {
        $this.app.getRootNav().setRoot(LoginPage, {showForm: true});
      }
    });
  }

  async deletePhotosFromLocalStorage() {
    try {
      let today = new Date();
      today.setHours(0,0,0,0);
      
      const keys = await this.storage.keys();
      for(var i=0; i<keys.length; i++) {
        const key = keys[i];
        
        if (key.indexOf('photos') !== -1) {
          let photos: any[] = await this.storage.get(key);
          photos = photos.filter(x => x.date == today.getTime() && x.progress !== 100);

          if (photos.length !== 0) {
            this.storage.set(key, photos);
          } else {
            this.storage.remove(key);
          }
        }
      }
    } catch (error) {
      
    }
  }
}
