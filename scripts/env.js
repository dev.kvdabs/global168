const fs = require("fs");

function getEnvironment() {
    for (var i = 0; i < process.argv.length; i++) {
      if (process.argv[i] === "--env") {
        if (process.argv[i + 1]) {
          return process.argv[i + 1];
        }
      }
    }
    return "dev";
}

const env = getEnvironment();

const content = fs.readFileSync(`./src/environments/environment.${env}.ts`);
fs.writeFileSync('./src/environments/environment.ts', content);